#include <vxWorks.h>
#include <stdio.h>
#include <taskLib.h>
#include <sysLib.h>
#include <semLib.h>
#include <logLib.h>
#include <msgQLib.h>
#include <time.h>
#include <tickLib.h>


#define MAX_MESSAGES 100
#define MAX_MESSAGE_LENGTH 50
#define DELAY_PERIOD sysClkRateGet(); // used by watchdog
#define TICK sysClkRateGet()/60
#define WALKRATE sysClkRateGet(); // 1 square per second used by timer task

void tDisplay();
void tLed();
void tSensor();

SEM_ID sem;
MSG_Q_ID mqPosition;
WDOG_ID wdId;
struct timespec ts;

struct{
    char topLed[10];
    char bottomLed[10];
    char position[10];
    //char * status[50];
}hallway;


// watchdogs watch the toggle ambient light
// signals if day then suspend the led if night resume led
// message queue of ints for position index
// mutex sem for led & sensor
// binary sem ?
// timestamp for status
// initial rate is person moves one square per second
void init(void){
    
    int taskDisplay, taskLed, taskSensor;
    
    int i;
      // start clock for timestamping
    ts.tv_sec = 0;
    ts.tv_nsec = 0;
    clock_settime(CLOCK_REALTIME, &ts);

	// initialize display
    for(i=0;i<10;i++){
   	 hallway.topLed[i] = '-';
   	 hallway.position[i] = '@';
   	 hallway.bottomLed[i] = '-';
    }

    // create and start watchdog timer
	wdId = wdCreate();

	//wdStart(wdId, DELAY_PERIOD, );
    // create objects
    //if((mqPosition = msgQCreate(MAX_MESSAGES, MAX_MESSAGE_LENGTH, MSG_Q_FIFO)==NULL));
    //    printf("msgQCreate in failed \n");
    sem = semMCreate(SEM_Q_FIFO);
	taskDisplay = taskSpawn("display", 95,0x100,2000,(FUNCPTR)tDisplay,0,0,0,0,0,0,0,0,0,0);
	taskLed = taskSpawn("led", 100,0x100,2000,(FUNCPTR)tLed,0,0,0,0,0,0,0,0,0,0);
	taskSensor = taskSpawn("sensor", 100,0x100,2000,(FUNCPTR)tSensor,0,0,0,0,0,0,0,0,0,0);


}

void tDisplay(void){

    // get clock
    // print three lines at a time that indicate
    // the leds and the current position

    while(1){
   	 semTake(sem, WAIT_FOREVER);
   	 int i, j, k;
   	 for(j=0;j<10;j++){
   			 	 
   	   for(i=0;i<10;i++){
   	   	printf("%c ",hallway.topLed[i]);}
   	   printf("\n");
   			       	 
   	   for(k=0;k<j;k++){printf("  ");}
   	   printf("%c \n", hallway.position[j]);
   			   	 
   	   for(i=0;i<10;i++){
   		   printf("%c ", hallway.bottomLed[i]);}
   	   printf("\n\n\n\n");
   			   	 
   	   taskDelay(sysClkRateGet());
   	 } //j loop    
   	 taskDelay(sysClkRateGet());
   	 semGive(sem);
    }
}


void tSensor(void){

    // senses movement per walkrate
    int i = 0;
    while(1){
   	 //clock_gettime(CLOCK_REALTIME, &ts);

   	 semTake(sem, WAIT_FOREVER);
   	 hallway.position[i]='@';    // updates the person display.position
   	 i = (i<9)? i++ : 0;
   	 semGive(sem);
    	//wdStart(wdId, DELAY_PERIOD, );    // restart wd
   	 // send position to message queue
   	 //msgQSend(mqPosition, i, MAX_MESSAGE_LENGTH, WAIT_FOREVER, MSG_PRI_NORMAL);
   	 taskDelay(60);    // delay one second

    }
}


void tLed(void){

    // receives position from message queue
    // updates led struct
    int i=0;
    while(1){
   	 //if((msgQReceive(mqPosition, message, MAX_MESSAGE_LENGTH, WAIT_FOREVER))== ERROR)
   	 //    printf("msgQReceive in tClient failed \n");
   	 //else    // print what was received
   		 // do something with message queue
   	 semTake(sem, WAIT_FOREVER);
   	 hallway.topLed[i]='+';
   	 hallway.bottomLed[i]='+';
   	 i = (i<9)? i++ : 0;
   	 semGive(sem);
   	 taskDelay(60);
    }

}

void toggleAmbient(int isDaytime){
    // start timer
    // toggle day after 24 secs
    // day = 1 night = 0
    // isDaytime == 1 ? send_signal(1) : send_signal(2);


}

void sigHandler(int signal){
    // receives signal to resume Led tasks

    switch(signal){
   	 case 1:{
   		 // suspend led tasks
   		 // create new watchdog for leds and sensor
   		 // start tasks
   	 }

   	 case 2:{
   		 //
   		 //
   	 }
    }

}
