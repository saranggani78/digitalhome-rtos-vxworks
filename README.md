Digital Home

This application demonstrates how embedded devices work together to capture where a person is in a hallway and illuminate that spot using leds.
It runs on devices with embedded VxWorks OS.
It has three concurrent worker threads (aka tasks): tDisplay, tLed, tSensor.
The worker threads share access to common data repositories: topLed, bottomLed, and currentPosition.
The data in the repositories represent information from a hallway.
The current position represents where one person walking through the hallway is relative to the sensor.

credits to:
 * Prof. Salamah Salamah, Dept of CS, UTEP

contributors:
 * Steve Gavil
 * Luis Guarnica
 * Cyrus Talladen