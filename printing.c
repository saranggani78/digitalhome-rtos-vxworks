#include <vxWorks.h>
#include <stdio.h>
#include <taskLib.h>
#include <sysLib.h>
#include <private\trgLibP.h>

int global_howmany = -1;

// print task id 
int printing(int arg){
	int k;
	for (k=0;k<arg;k++){
		printf("I am task %d, iter: #%d\n",taskIdSelf(), k);
		taskDelay(sysClkRateGet()/60); // 1/60 s delay

	}

	if(arg==global_howmany){
		trgEvent(40010);
		global_howmany =-1;
	}

	return 0;
}

// spawn tasks
// call the printing function
void spawn_tasks(int howmany){
	int i, taskId;
	global_howmany = howmany;
	for(i=1;i<= howmany;i++){
		taskId = taskSpawn(NULL,90,0x100,2000,(FUNCPTR)printing, i);
	}


}
