void Sensor(int);
void Display(int);

#define TICK sysClkRateGet()/60
#define ITER 22
SEM_ID semMtx;
int taskSensor, taskDisplay;

void createM(){
    semBin = semMCreate(SEM_Q_FIFO , 0);
    
}

// TODO main routine
void binary(int protect){
    // clear out members
    data.x = 0;
    data.y = 0;
    data.z = 0;

    taskDisplay = taskSpawn("td", 95,0x100,2000,(FUNCPTR)Display,protect,0,0,0,0,0,0,0,0,0);
    taskSensor = taskSpawn("tsp", 95,0x100,2000,(FUNCPTR)Sensor,protect,0,0,0,0,0,0,0,0,0);
    taskDelay(220*TICK);
    taskDelete(taskDisplay);
}

// TODO display uses timestamp tick instead of millisec
void Display(int protect){
    
    struct timespec tpstart, tpend;
    int count=0,isec, insec, milli_sec;
    int ticks;
    
    clock_gettime(CLOCK_REALTIME, &tpstart);
    
    while(1){
   	 if(protect)
   		 semTake(semBin, WAIT_FOREVER);
   	 ticks = (int)sysTimestamp();
   	 logMsg("display #%d --> %d %d %d at %d ticks  \n",
   					 count++, data.x, data.y, data.z, ticks, NULL);
   	 if(protect) semGive(semBin);
   	 data.x = 0;  data.y = 0; data.z = 0;
   	 taskDelay(22*TICK);
    } 
}
// TODO sensor
void Sensor(int protect){
    int i;
    for(i=0; i<ITER;i++){
   	 if(protect)
   		 semTake(semBin, WAIT_FOREVER);
   	 data.x++;
   	 taskDelay(1*TICK);
   	 data.y++;
   	 taskDelay(6*TICK);
   	 data.z++;
   	 taskDelay(3*TICK);   	 
   	 //block
   	 if(protect)
   		 semGive(semBin);
    }
}
