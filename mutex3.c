#include <vxWorks.h>
#include <time.h>
#include <taskLib.h>
#include <sysLib.h>
#include <semLib.h>
#include <logLib.h>

#define NANOS_IN_SEC 1000000000
#define NANOS_PER_MILLI 1000000
#define TICK sysClkRateGet()/60

void SensorP(int);
void SensorM(int);
void Display(int);

#define ITER 22
SEM_ID semMtx;

int taskSensorP, taskSensorM, taskDisplay;

struct mem{
	int x;
	int y;
	int z;

}data;

void createM(){
	semMtx = semMCreate(SEM_Q_FIFO | SEM_DELETE_SAFE);
	
}

// main routine
void mutex(int protect){
	// clear out members
	data.x = 0;
	data.y = 0;
	data.z = 0;

	taskDisplay = taskSpawn("td", 95,0x100,2000,(FUNCPTR)Display,protect,0,0,0,0,0,0,0,0,0);
	taskSensorP = taskSpawn("tsp", 95,0x100,2000,(FUNCPTR)SensorP,protect,0,0,0,0,0,0,0,0,0);
	taskSensorM = taskSpawn("tsm", 95,0x100,2000,(FUNCPTR)SensorM,protect,0,0,0,0,0,0,0,0,0);
	taskDelay(220*TICK);
	//taskDelay(0);
	taskDelete(taskDisplay);
}

void Display(int protect){
	
	struct timespec tpstart, tpend;
	int count=0,isec, insec, milli_sec;
	
	clock_gettime(CLOCK_REALTIME, &tpstart);
	
	while(1){
		if(protect) 
			semTake(semMtx, WAIT_FOREVER);
		
		clock_gettime(CLOCK_REALTIME, &tpstart);
		isec = tpend.tv_sec - tpstart.tv_sec;
		insec = tpend.tv_nsec - tpstart.tv_sec;
		if(insec < 0){
			insec = insec + NANOS_IN_SEC;
			isec--;
		}
		milli_sec = insec/NANOS_PER_MILLI;
		
		logMsg("display #%d --> %d %d %d at %d sec and %d millisec \n", 
				count++, data.x, data.y, data.z, isec, milli_sec);
		
		if(protect) 
			semGive(semMtx);
		
		data.x = 0;
		data.y = 0;
		data.z = 0;
		
		taskDelay(22*TICK);
		//taskDelete(taskDisplay);

	}
	
}


void SensorP(int protect){
	int i;
	for(i=0; i<ITER;i++){
		
		if(protect) 
			semTake(semMtx, WAIT_FOREVER);
		data.x++;
		taskDelay(7*TICK);
		data.y++;
		taskDelay(1*TICK);
		data.z++;
		taskDelay(2*TICK);		
		
		//block
		if(protect) 
			semGive(semMtx);
	}
	
}

void SensorM(int protect){
	int i;
	for(i=0;i<ITER;i++){
		if(protect) semTake(semMtx, WAIT_FOREVER);
			data.x--;
			taskDelay(1*TICK);
			data.y--;
			taskDelay(6*TICK);
			data.z--;
			taskDelay(3*TICK);
			
			if(protect) semGive(semMtx);
	}
	
	
}
