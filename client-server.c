#include <vxWorks.h>
#include <stdio.h>
#include <taskLib.h>
#include <sysLib.h>
#include <msgQLib.h>

void tStamp(int);
void tClient(int);
void tServer(void);

#define MAX_MESSAGES 100
#define MAX_MESSAGE_LENGTH 50

#define CLOCK_REALTIME 0x0
// globals

MSG_Q_ID mqId1;
MSG_Q_ID mqId2;

// mutex semaphore 

struct timespec{

  time_t tv_sec;
  time_t tv_nsec;

}tstamp;

// create message queue and spawn two tasks
void message(void){

  int clientId, serverId, stampId;


  if((mqId = msgQCreate(MAX_MESSAGES, MAX_MESSAGE_LENGTH, MSG_Q_FIFO))==NULL)
	printf("msgQCreate in failed \n");

  if((mqId2 = msgQCreate(MAX_MESSAGES, MAX_MESSAGE_LENGTH, MSG_Q_FIFO))==NULL)
	printf("msgQCreate in failed \n");

  // client with rate of 1 message per second
  if((clientId = taskSpawn("client1",110, 0x100, 2000, (FUNCPTR)tClient,60,0,0,0,0,0,0,0,0,0))== ERROR)
	printf("taskSpawn of t1 in failed \n");

  if((serverId = taskSpawn("server",110, 0x100, 2000, (FUNCPTR)tServer,0,0,0,0,0,0,0,0,0,0))== ERROR)
	printf("taskSpawn of t1 in failed \n");


//  if((stampId = taskSpawn("t3",110, 0x100, 2000, (FUNCPTR)tStamp,sysClkRateGet(),0,0,0,0,0,0,0,0,0))==ERROR)
	// printf("taskSpawn of t3 in failed \n");

}


// client task that sends message to server
void tClient(int rate){
	
	char message[MAX_MESSAGE_LENGTH];
	char msgBuf[MAX_MESSAGE_LENGTH];

	tstamp.tv_sec = 0;
	tstamp.tv_nsec = 0;

	// start clock
	clock_settime(CLOCK_REALTIME, &tstamp);
	int i =0;
	while(1){

	// sem take

	taskDelay((sysClkRateGet()*rate)/60);	// delay rate ticks

	// get real clock timestamp
	clock_gettime(CLOCK_REALTIME, &tstamp);


	sprintf( message, "message # %d from CLIENT %d", i, taskIdSelf());
	printf(" CLIENT %d MESSAGE %d : \n", taskIdSelf(), i++);     // print what is sent
	// print timestamp or send timestamp?
	// 	printf("\ntime: %d sec %d nsec", (int)tstamp.tv_sec, (int)tstamp.tv_nsec);


  	if((msgQSend(mqId, message, MAX_MESSAGE_LENGTH, WAIT_FOREVER, MSG_PRI_NORMAL))== ERROR)
		printf("msgQSend in tSender failed \n");

	// receive response
	if((msgQReceive(mqId2, msgBuf, MAX_MESSAGE_LENGTH, WAIT_FOREVER))== ERROR)
		printf("msgQReceive in tClient failed \n");
	else	// print what was received 
		printf("CLIENT %d : %s\n",taskIdSelf(), msgBuf);

	// sem give
  	}
	
}


// server task that echoes back to client
void tServer(void){

  char message[MAX_MESSAGE_LENGTH];
  //char msgBuf[MAX_MESSAGE_LENGTH];
  tstamp.tv_sec = 0;
  tstamp.tv_nsec = 0;
  
  // start clock setclock
  int i =0;
  while(1){

  	// sem take

	taskDelay(sysClkRateGet()/60);	// delay 1/60 second = 1 tick


  	// get clock stamp
  	clock_gettime(CLOCK_REALTIME, &tstamp);

	// 	printf("\ntime: %d sec %d nsec", (int)tstamp.tv_sec, (int)tstamp.tv_nsec);
  	if((msgQReceive(mqId, message, MAX_MESSAGE_LENGTH, WAIT_FOREVER))== ERROR)
		printf("msgQReceive in tServer failed \n");
	else	// print what was received 
		printf("SERVER %d : %s\n",taskIdSelf(), message);

	sprintf( message, "message # %d from SERVER %d", i, taskIdSelf());
	//printf(" SENDER %d MESSAGE %d : \n", taskIdSelf(), i++);     // print what is sent

  	if((msgQSend(mqId2, message, MAX_MESSAGE_LENGTH, WAIT_FOREVER, MSG_PRI_NORMAL))== ERROR)
		printf("msgQSend in tServer failed \n");

	// sem give

  }
	
}



// initializes the clock and prints out time in sec/nsec every step time ticks
void tStamp(int step){
  
  tstamp.tv_sec = 0;
  tstamp.tv_nsec = 0;
  
  clock_settime(CLOCK_REALTIME, &tstamp);
  
  while(1){

  	// taskDelay(sysClkRateGet());  //delay 1 sec
	taskDelay(step);

	clock_gettime(CLOCK_REALTIME, &tstamp);

	printf("\ntime: %d sec %d nsec", (int)tstamp.tv_sec, (int)tstamp.tv_nsec);
  }

}

