#include <vxWorks.h>
#include <stdio.h>
#include <taskLib.h>
#include <sysLib.h>
#include <msgQLib.h>

void tSender(void);
void tReceiver(void);
void tStamp(int);

#define MAX_MESSAGES 100
#define MAX_MESSAGE_LENGTH 50

#define CLOCK_REALTIME 0x0
// globals

MSG_Q_ID mqId;

struct timespec{

  time_t tv_sec;
  time_t tv_nsec;

}tstamp;

// create message queue and spawn two tasks
void message(void){
  int senderId, receiverId, stampId;
  
  if((mqId = msgQCreate(MAX_MESSAGES, MAX_MESSAGE_LENGTH, MSG_Q_FIFO))==NULL)
	printf("msgQCreate in failed \n");

  if((senderId = taskSpawn("t1",110, 0x100, 2000, (FUNCPTR)tSender,0,0,0,0,0,0,0,0,0,0))== ERROR)
	printf("taskSpawn of t1 in failed \n");

  if((receiverId = taskSpawn("t2",110, 0x100, 2000, (FUNCPTR)tReceiver,0,0,0,0,0,0,0,0,0,0))==ERROR)
	printf("taskSpawn of t2 in failed \n");
  
//  if((stampId = taskSpawn("t3",110, 0x100, 2000, (FUNCPTR)tStamp,sysClkRateGet(),0,0,0,0,0,0,0,0,0))==ERROR)
//	printf("taskSpawn of t3 in failed \n");

}

// task that writes to the message queue
void tSender(void){
  
  char message[MAX_MESSAGE_LENGTH];
  int i =0;
  
  while(1){
	sprintf( message, "message # %d from Sender %d", i, taskIdSelf());
	printf(" SENDER %d MESSAGE %d : \n", taskIdSelf(), i++);     // print what is sent

  	if((msgQSend(mqId, message, MAX_MESSAGE_LENGTH, WAIT_FOREVER, MSG_PRI_NORMAL))== ERROR)
		printf("msgQSend in tSender failed \n");

	taskDelay(sysClkRateGet());	// delay one second
  }
}

// task that reads from the message queue
void tReceiver(void){

  char msgBuf[MAX_MESSAGE_LENGTH];

  while(1){
	
	if((msgQReceive(mqId, msgBuf, MAX_MESSAGE_LENGTH, WAIT_FOREVER))== ERROR)
		printf("msgQSend in tSender failed \n");
	
	else	// print what was received 
		printf("RECIEVER %d : %s\n",taskIdSelf(), msgBuf);
	
	taskDelay(sysClkRateGet()/60);	// delay for 1/60 sec -> i tick
  }
}

// initializes the clock and prints out time in sec/nsec every step time ticks
void tStamp(int step){
  
  tstamp.tv_sec = 0;
  tstamp.tv_nsec = 0;
  
  clock_settime(CLOCK_REALTIME, &tstamp);
  
  while(1){

	taskDelay(step);

	clock_gettime(CLOCK_REALTIME, &tstamp);

	printf("\ntime: %d sec %d nsec", (int)tstamp.tv_sec, (int)tstamp.tv_nsec);
  }

}

